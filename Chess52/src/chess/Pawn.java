package chess;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
/**
 * 
 * @author Shaan Kalola and Tom Murphy
 */
public class Pawn extends Piece{
	
	/**
	 * field to indicate if the last move the pawn made was a double move
	 */
	public boolean lastMovedouble = false; 
	
	/**
	 * 
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param numMoves gives the number of Moves the piece made
	 * @param name gives the type of piece 
	 * @param color gives the color of the piece
	 */
	public Pawn(int row, int col, int numMoves, String name, String color) {
		super(row, col, numMoves, name, color);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param color gives the color of the piece
	 */
	public Pawn (int row, int col, String color) {
		this(row,col,0,"pawn", color);
	}

	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @return returns true if the piece can move from original to newPair
	 */
	public boolean isValid(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		if(newPair.getRow()< 0 || newPair.getRow() > 7 || newPair.getCol() < 0 || newPair.getCol() > 7) { //checks if newPair in the board
			return false;
		}
		
		//checks that piece doesn't land on a position with a piece of the same color 
		if(coordinates.get(newPair) != null && coordinates.get(newPair).color.equals(this.color)) { 
			return false;
		}
		//checks if move is en_passant
		if(en_passant(coordinates,original,newPair)) {
			return true;
		}
		//checks double Move
		if(doubleMove(coordinates, original, newPair)) {
			return true;
		}
		if(attack(coordinates,original,newPair)) {
			return true;
		}
		
		if(coordinates.get(newPair) != null) {
			return false;
		}
		
		int row = newPair.getRow() - original.getRow();
		int col = newPair.getCol() - original.getCol();
		
		if(col != 0) { //checks that pawn remains in file
			return false;
		}
		
		
		if(this.color.equals("white")) {
			if(row != -1) { //if piece is white checks that pawn moves up one rank
				return false;
			}
		
		}
		
		if (this.color.equals("black")) {
			if(row != 1) { //if piece is black checks that pawn moves down one rank
				return false;
			}
		}
		
		
		
		return true;
	}


	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @return returns true if the Pawn is making an en_passant move 
	 */
	public boolean en_passant (HashMap<Pair,Piece> coordinates, Pair original, Pair newPair) {
		//if newPair not in board returns false
		if(newPair.getRow()< 0 || newPair.getRow() > 7 || newPair.getCol() < 0 || newPair.getCol() > 7) {
			return false;
		}
		//if a piece of the same color is on the newPair coordinates returns false
		if(coordinates.get(newPair) != null && coordinates.get(newPair).color.equals(this.color)) {
			return false;
		}
		
		int row = newPair.getRow() - original.getRow();
		int col = newPair.getCol() - original.getCol();
		if(col != 1 && col != -1) { //checks that the pawn moved left or right
			return false;
		}
		
		if(this.color.equals("white")) { //checks that if the pawn is white it moved up one rank
			if(row != -1) {
				return false;
			}			
		}
		
		if(this.color.equals("black")) { //checks that if the pawn is black it moved down one rank
			if(row != 1) {
				return false;
			}			
		}
		Piece piece = coordinates.get(new Pair(original.getRow(),newPair.col)); //checks if there is a piece where the Pawn is passing
		if(piece == null) {
			return false;
		}
		if(!(piece instanceof Pawn)) { //if the piece is not a pawn returns false
			return false;
		}
		Pawn pawn = (Pawn) piece;
		if(piece.color.equals(this.color)) { //checks that the pawn is not the same color
			return false;
		}
		if(!pawn.lastMovedouble) { //checks that the last move the pawn made was a double move
			return false;
		}
		
		
		return true;
	}
	
	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @return returns true if the Pawn is making a double move
	 */
	
	public boolean doubleMove (HashMap<Pair, Piece> coordinates, Pair original, Pair newPair ) {
		if(this.numMoves != 0) { //returns false if the pawn has made a move
			return false;
		}
		int row = newPair.getRow() - original.getRow();
		int col = newPair.getCol() - original.getCol();
		
		if(col != 0) { //checks that the pawn is in the same file
			return false;
		}
		
		if(this.color.equals("white")) {
			if(row != -2) { //checks that the pawn moved up 2 ranks if color is white
				return false;
			}
		}
		
		if(this.color.equals("black")) {
			if(row != 2) { //checks that the pawn moved down 2 ranks if color is black
				return false;
			}
		}
		
		Piece obs1 = coordinates.get(new Pair (original.getRow() + row/2, original.getCol())); //checks for not skipping pieces 
		if(obs1 != null) {
			return false;
		}
		
		Piece obs2 = coordinates.get(new Pair (original.getRow() + row, original.getCol()));
		
		if(obs2 != null) { //checks that there is not a piece at the newPair
			return false;
		}
		
		return true;
	}
	/**
	 * 
	 * @param newPair coordinates of the Pawn 
	 * @return returns true if the Pawn should be promoted
	 */
	public boolean promotion (Pair newPair) {
		if(this.row == 7 && this.returnColor().equals("black")) {
			return true;
		}
		if(this.row == 0 && this.returnColor().equals("white")) {
			return true;
		}
		
		
		return false;
	}
	/**
	 * 
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @return returns true if the Pawn is making an attack
	 */
	public boolean attack (HashMap<Pair,Piece> coordinates, Pair original, Pair newPair) {
		if(coordinates.get(newPair) != null && coordinates.get(newPair).color.equals(this.color)) {
			return false;
		}
		
		if(coordinates.get(newPair) == null) {
			return false;
		}
		int row = newPair.getRow() - original.getRow();
		int col = newPair.getCol() - original.getCol();
		
		if(this.color.equals("white")) { //checks that if the pawn is white it moved up one rank
			if(row != -1) {
				return false;
			}			
		}
		if (this.color.equals("black")) {
			if(row != 1) { //if piece is black checks that pawn moves down one rank
				return false;
			}
		}
		
		if(col != 1 && col != -1) {
			return false;
		}
		
		
		
		return true;
	}
	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @param board int [][] that contains 1 where there is a piece and 0 where there is not
	 updates coordinates 
	 */
	public void executeMove (HashMap<Pair,Piece> coordinates, Pair original, Pair newPair, int [] [] board) {
		if(en_passant(coordinates, original, newPair)) { //checks if the move is en_passant
			coordinates.remove(original); //removes the piece from the board
			coordinates.put(newPair, this); //puts piece on the new position on the coordinates
			Piece toremove = coordinates.remove(new Pair(original.row, newPair.col )); //removes the piece take by en_passent
			board[original.getRow()][original.getCol()] = 0; //sets original position to zero on board
			board[original.getRow()][newPair.getCol()] = 0; //sets the position of piece taken by en_passant to 0 
			board[newPair.getRow()][newPair.getCol()] = 1; //sets the new position to one on the board
			this.row = newPair.getRow(); //sets the row of piece to new position
			this.col = newPair.getCol(); //sets the column of piece to the new position
			this.numMoves += 1; //add one to number of moves
			return;
		}
		if(doubleMove(coordinates,original,newPair)) { //checks if doubleMove
			coordinates.remove(original);//removes the piece from the board
			coordinates.put(newPair, this);//puts piece on the new position on the coordinates
			board[original.getRow()][original.getCol()] = 0; //sets original position to zero on coordinates
			board[newPair.getRow()][newPair.getCol()] = 1;//sets the new position to one on the board
			this.row = newPair.getRow(); //sets the row of piece to new position
			this.col = newPair.getCol();//sets the column of piece to the new position
			this.numMoves += 1;  //add one to number of moves
			this.lastMovedouble = true; //sets lastMovedouble to true
			return;
		}
		coordinates.remove(original); //removes the piece from the coordinates
		coordinates.put(newPair, this);//puts piece on the new position on the coordinates
		board[original.getRow()][original.getCol()] = 0; //sets original position to zero on board
		board[newPair.getRow()][newPair.getCol()] = 1; //sets the new position to one on the board
		this.row = newPair.getRow(); //sets the row of piece to new position
		this.col = newPair.getCol(); //sets the column of piece to the new position
		this.numMoves += 1; //add one to number of moves
		
	}
	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 updates coordinates 
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		if(en_passant(coordinates, original, newPair)) { //checks if the move is en_passant
			coordinates.remove(original); //removes the piece from the coordinates
			coordinates.put(newPair, this); //puts piece on the new position on the coordinates
			coordinates.remove(new Pair(original.row, newPair.col )); //removes the Piece captured by en_passant
			return;
		}
		if(doubleMove(coordinates,original,newPair)) { //checks if double Move
			coordinates.remove(original); //removes the piece from coordinates
			coordinates.put(newPair, this); //add the piece to coordinates
			return;
		}
		coordinates.remove(original); //removes the piece from coordinates
		coordinates.put(newPair, this);//add the piece to coordinates
	}

}
