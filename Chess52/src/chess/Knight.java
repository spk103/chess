package chess;

import java.util.HashMap;
import java.util.HashSet;
/**
 * 
 * @author Shaan Kalola and Tom Murphy
 */
public class Knight extends Piece{
	/**
	 * Set of Pairs used to represent movements of the Knight
	 */
	HashSet <Pair> moves = new HashSet<Pair>();
	/**
	 * 
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param numMoves gives the number of Moves the piece made
	 * @param name gives the type of piece 
	 * @param color gives the color of the piece
	 */
	public Knight(int row, int col, int numMoves, String name, String color) {
		super(row, col, numMoves, name, color);
		fillSet();
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param color gives the color of the piece
	 */
	public Knight (int row, int col, String color) {
		this(row,col, 0, "knight", color);
	}
	/**
	 * fills moves HashSet with possible moves
	 */
	public void fillSet() {
		moves.add(new Pair (-1,-2));
		moves.add(new Pair(1,-2));
		moves.add(new Pair (2,-1));
		moves.add(new Pair (2,1));
		moves.add(new Pair (1,2));
		moves.add(new Pair (-1,2));
		moves.add(new Pair (-2,1));
		moves.add(new Pair (-2,-1));
	}
	
	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @return returns true if the piece can move from original to newPair
	 */
	public boolean isValid(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		//checks if newPair is valid
		if(newPair.getRow()<0 || newPair.getRow() > 7 || newPair.getCol() < 0 || newPair.getCol() > 7) {
			return false;
		}
		//checks if there is a piece at newPair that has the same color as the Knight
		if(coordinates.get(newPair) != null && coordinates.get(newPair).color.equals(this.color)) {
			return false;
		}
		//Creates pair to represent movement from original to newPair
		Pair pair = new Pair (newPair.getRow() - original.getRow(), newPair.getCol() - original.getCol());
		
		//Checks if move is valid
		if(this.moves.contains(pair)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @param board int [][] that contains 1 where there is a piece and 0 where there is not
	 updates coordinates 
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair, int[][] board) {
		// TODO Auto-generated method stub
		//updates coordinates map
		coordinates.remove(original);
		coordinates.put(newPair, this);
		//updates board
		board[original.getRow()][original.getCol()] = 0;
		board[newPair.getRow()][newPair.getCol()] = 1;
		//updates fields
		this.row = newPair.getRow();
		this.col = newPair.getCol();
		this.numMoves += 1;
	}
	
	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 updates coordinates 
	 this method is meant to use on dummy HashMaps that are copies of coordinates, as it doesn't update board, or number of moves
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		//updates coordinates map
		coordinates.remove(original);
		coordinates.put(newPair, this);
		
	}
	
	

}
