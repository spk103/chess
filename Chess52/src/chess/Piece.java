package chess;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
/**
 * 
 * @author Shaan Kalola and Tom Murphy
 */
public abstract class Piece {
	/**
	 * row gives the row where the Piece is on the coordinates
	 */
	public int row;
	/**
	 * col gives the column where the Piece is on the coordinates
	 */
	public int col;
	/**
	 * numMoves gives the number of Moves the piece made
	 */
	public int numMoves;
	/**
	 * name gives the type of piece
	 */
	public String name;
	/**
	 * color gives the color of the piece
	 */
	public String color;
	/**
	 * 
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param numMoves gives the number of Moves the piece made
	 * @param name gives the type of piece 
	 * @param color gives the color of the piece
	 */
	public Piece(int row, int col, int numMoves, String name, String color) {
		this.row = row;
		this.col = col;
		this.numMoves = numMoves;
		this.name = name;
		this.color = color;
	}
	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @return returns true if the piece can move from original to newPair
	 */
	public abstract boolean isValid (HashMap<Pair,Piece> coordinates, Pair original, Pair newPair);
	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @param board int [][] that contains 1 where there is a piece and 0 where there is not
	 updates coordinates 
	 */
	public abstract void executeMove (HashMap<Pair,Piece> coordinates, Pair original, Pair newPair, int [] [] board);
	
	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 updates coordinates 
	 this method is meant to use on dummy HashMaps that are copies of coordinates, as it doesn't update board, or number of moves
	 */
	public abstract void executeMove (HashMap<Pair,Piece> coordinates, Pair original, Pair newPair);
	/**
	 * 
	 * @param row row of Piece
	 * @param col col of Piece
	 */
	public void setCoordinates (int row, int col) {
		this.row = row;
		this.col = col;
	}
	/**
	 * 
	 * @return returns number of Moves
	 */
	public int returnNumMoves() {
		return numMoves;
	}
	/**
	 * 
	 * @return returns type of piece
	 */
	public String returnName() {
		return name;
	}
	
	/**
	 * 
	 * @return returns Color of piece 
	 */
	public String returnColor() {
		return color;
	}
	
	/**
	 * 
	 * @return returns a Pair Object that contains the coordinates of the piece 
	 */
	public Pair returnPair() {
		return new Pair(row,col);
	}
	/**
	 * 
	 * @return returns row of Piece
	 */
	public int returnRow() {
		return row;
	}
	/**
	 * 
	 * @return returns col of Piece
	 */
	public int returnCol() {
		return col;
	}
	 /**
	  * 
	  * @param coordinates a HashMap that represents the game board
	  * @param original a Pair object that represents the original position of the piece
	  * @return returns a Set of Pairs that the piece can move to
	  */
	public Set<Pair> computeAllValidMoves(HashMap<Pair, Piece> coordinates, Pair original) {
		// TODO Auto-generated method stub
		Set <Pair> validPairs = new HashSet<Pair>();
		
		for (int i =0; i < 8; i++) {
			for(int j = 0; j < 8; j++) { //loops through all the coordinates
				if(this.isValid(coordinates, original, new Pair(i,j))) { //checks if path is valid
					validPairs.add(new Pair(i,j));
				}
			}
		}
		
		return validPairs;
	}
}
