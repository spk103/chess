package chess;

import java.util.HashMap;
/**
 * 
 * @author Shaan Kalola and Tom Murphy
 */
public class Queen extends Piece{
	/**
	 * 
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param numMoves gives the number of Moves the piece made
	 * @param name gives the type of piece 
	 * @param color gives the color of the piece
	 */
	public Queen(int row, int col, int numMoves, String name, String color) {
		super(row, col, numMoves, name, color);
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param color gives the color of the piece
	 */
	public Queen (int row, int col, String color) {
		this(row, col, 0, "queen", color);
	}

	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @return returns true if the piece can move from original to newPair
	 */
	public boolean isValid(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		
		//checks if newPair in board
		if(newPair.getRow()<0 || newPair.getRow() > 7 || newPair.getCol() < 0 || newPair.getCol() > 7) {
			return false;
		}
		//checks that there isn't a piece of same color at newPair
		if(coordinates.get(newPair) != null && coordinates.get(newPair).color.equals(this.color)) {
			return false;
		}
		//creates a pair that represents movement from original to newPair
		Pair pair = new Pair (newPair.getRow() - original.getRow(), newPair.getCol() - original.getCol());
		
		//checks if diagonal movement
		if(Math.abs(pair.getCol()) == Math.abs(pair.getRow())) {
			int boundary = Math.abs(pair.getRow());
			int row = pair.getRow() / boundary;
			int col = pair.getCol() / boundary;
			
			//checks no pieces on the path to newPair
			for(int i = 1; i < boundary; i++) {
				Pair p = new Pair(row * i + original.getRow(), col * i + original.getCol());
				Piece piece = coordinates.get(p);
				if(piece != null) {
					return false;
				}
			}
			
			//checks that there isn't a piece of same color at newPair
			Pair p = new Pair(row * boundary + original.getRow(), col * boundary + original.getCol());
			Piece piece = coordinates.get(p);
			if(piece != null && piece.color.equals(this.color)) {
				return false;
			}
			
			return true;
			
		}
		 //checks for rank movement
		if(pair.getRow() == 0) {
			if(pair.getCol() < 0) { //checks for negative movement along rank
				int boundary = pair.getCol();
				int i = -1;
				//checks no piece on the path
				while(i >= boundary + 1) {
					if(coordinates.get(new Pair (original.getRow(), original.getCol() + i)) != null){
						return false;
					}
					i -= 1;
				}
				//checks no piece on newPair of same color 
				Piece piece =coordinates.get(new Pair (original.getRow(), original.getCol() + boundary));
				if(piece != null && piece.color.equals(this.color)) {
					return false;
				}
				return true;
			}
			if(pair.getCol() > 0) { //checks for positive movement along rank
				int boundary = pair.getCol();
				int i = 1;
				//checks no piece on the path
				while(i <= boundary-1) {
					if(coordinates.get(new Pair (original.getRow(), original.getCol() + i)) != null){
						return false;
					}
					i += 1;
				}
				//checks no piece on newPair of same color 
				Piece piece =coordinates.get(new Pair (original.getRow(), original.getCol() + boundary));
				if(piece != null && piece.color.equals(this.color)) {
					return false;
				}
				return true;
			}
			return false;
		}
		
		if(pair.getCol() == 0) { //checks for file movements
			if(pair.getRow() < 0) { //checks for negative file movement
				int boundary = pair.getRow();
				int i = -1;
				//checks for pieces in path
				while(i >= boundary + 1) {
					if(coordinates.get(new Pair (original.getRow()+i, original.getCol())) != null){
						return false;
					}
					i -= 1;
				}
				//checks that no piece at newPair with same color
				Piece piece = coordinates.get(new Pair (original.getRow()+boundary, original.getCol()));
				if(piece != null && piece.color.equals(this.color)) {
					return false;
				}
				return true;
			}
			if(pair.getRow() > 0) { //checks for positive file movement
				int boundary = pair.getRow();
				int i = 1;
				//checks for pieces in path
				while(i <= boundary - 1) {
					if(coordinates.get(new Pair (original.getRow()+i, original.getCol())) != null){
						return false;
					}
					i += 1;
				}
				//checks that no piece at newPair with same color
				Piece piece = coordinates.get(new Pair (original.getRow()+boundary, original.getCol()));
				if(piece != null && piece.color.equals(this.color)) {
					return false;
				}
				return true;
			}
		}
		
		return false;
	}

	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @param board int [][] that contains 1 where there is a piece and 0 where there is not
	 updates coordinates 
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair, int[][] board) {
		// TODO Auto-generated method stub
		//updates coordinates board
		coordinates.remove(original);
		coordinates.put(newPair, this);
		//updates board
		board[original.getRow()][original.getCol()] = 0;
		board[newPair.getRow()][newPair.getCol()] = 1;
		//updates fields 
		this.row = newPair.getRow();
		this.col = newPair.getCol();
		this.numMoves += 1;
	}

	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 updates coordinates 
	 this method is meant to use on dummy HashMaps that are copies of coordinates, as it doesn't update board, or number of moves
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		//updates coordinates
		coordinates.remove(original);
		coordinates.put(newPair, this);
	}

}
