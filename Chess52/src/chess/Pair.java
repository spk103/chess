package chess;
/**
 * 
 * @author Shaan Kalola and Tom Murphy
 */
public class Pair  {
	/**
	 * row field that represents rank in integer format
	 */
	
	public int row;
	
	/**
	 * col field that represents rank in integer format
	 */
	public int col;
	
	/**
	 * 
	 * @param row field that represents rank in integer format
	 * @param col field that represents col in integer format
	 */
	public Pair(int row, int col) {
		this.row = row;
		this.col = col;
	}
	
	/**
	 * returns and overrides hashCode
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + row;
		return result;
	}

	/**
	 * @param obj is the object checked for equality
	 * overwrites equals
	 * 
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	/**
	 * 
	 * @return returns row parameter
	 */
	public int getRow() {
		return row;
	}
	/**
	 * 
	 * @return returns col parameter
	 */
	public int getCol() {
		return col;
	}
	
}
