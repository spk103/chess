package chess;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
/**
 * 
 * @author Shaan Kalola and Tom Murphy
 *
 */
public class King extends Piece{
	/**
	 * inCheck is used to determine if King is in Chess
	 */
	boolean inCheck = false; //field to keep track of when king is in check 
	
	/**
	 * Set of Pairs used to store the possible moves of the King 
	 */
	HashSet<Pair> moves = new HashSet <Pair>();
	
	/**
	 * @param row the row where the Piece is on the coordinates
	 * @param col the column where the Piece is on the coordinates
	 * @param numMoves the number of Moves the piece made
	 * @param name the type of piece 
	 * @param color the color of the piece
	 */
	public King(int row, int col, int numMoves, String name, String color) {
		super(row, col, numMoves, name, color);
		setFill(); // fills moves Set with Pairs
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 * @param row the row where the Piece is on the coordinates
	 * @param col the column where the Piece is on the coordinates
     * @param color the color of the piece
	 */
	public King(int row, int col, String color) {
		this(row,col,0,"king", color);
	}
	/**
	 * fills moves set
	 */
	public void setFill () { //Makes a set of Pairs with Valid moves for King
		moves.add(new Pair (-1,-1));
		moves.add(new Pair(-1,0));
		moves.add(new Pair(-1,1));
		moves.add(new Pair(0,-1));
		moves.add(new Pair(0,1));
		moves.add(new Pair(1,-1));
		moves.add(new Pair(1,0));
		moves.add(new Pair(1,1));
	}

	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @return returns true if the piece can move from original to newPair
	 */
	public boolean isValid(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		//checks if newPair in the board
		if(newPair.getRow()<0 || newPair.getRow() > 7 || newPair.getCol() < 0 || newPair.getCol() > 7) { 
			return false;
		}
		
		//checks if King is Castling
		if(this.isCastling(coordinates, original, newPair)) { 
			return true;
		}
		
		//checks that piece doesn't land on a position with a piece of the same color
		if(coordinates.get(newPair) != null && coordinates.get(newPair).color.equals(this.color)) {  
			return false;
		}
		
		
		//checks if the move is in the moves HashSet
		if(this.moves.contains(new Pair(newPair.getRow() - original.getRow(), newPair.getCol() - original.getCol()))) {
			return true;
		}
		
		return false;
	}


	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair, int[][] board) {
		// TODO Auto-generated method stub
		if(this.isCastling(coordinates,original,newPair)) { //checks castling
			if(original.equals(new Pair (0,4))) { // checks for black king 
				if(newPair.equals(new Pair(0,6))) { //checks for right castling
					Piece rook = coordinates.get(new Pair(0,7)); //gets rook
					
					//updates coordinates HashMap 
					coordinates.remove(new Pair(0,7));
					coordinates.put(new Pair(0,5), rook);
					coordinates.remove(new Pair(0,4));
					coordinates.put(new Pair(0,6), this);
					
					//updates board
					board[0][7] = 0;
					board[0][5] = 1;
					board[0][4] = 0;
					board[0][6] = 1;
					
					//updates fields of King after move
					row = newPair.getRow();
					col = newPair.getCol();
					numMoves += 1;
					return;
				}
				if(newPair.equals(new Pair(0,2))){ //checks for left castling
					Piece rook = coordinates.get(new Pair(0,0)); //checks if rook at (0,0)
					//updates coordinates HashMap
					coordinates.remove(new Pair(0,0));
					coordinates.put(new Pair(0,3), rook);
					coordinates.remove(new Pair(0,4));
					coordinates.put(new Pair(0,2), this);
					//updates board
					board[0][0] = 0;
					board[0][3] = 1;
					board[0][4] = 0;
					board[0][2] = 1;
					//updates fields of King
					row = newPair.getRow();
					col = newPair.getCol();
					numMoves += 1;
					return;
				}
			}
			
			if(original.equals(new Pair (7,4))) { //checks for White
				if(newPair.equals(new Pair(7,6))) { //checks for Right castling
					Piece rook = coordinates.get(new Pair(7,7)); //checks if right rook there
					//updates coordinates 
					coordinates.remove(new Pair(7,7)); 
					coordinates.put(new Pair(7,5), rook);
					coordinates.remove(new Pair(7,4));
					coordinates.put(new Pair(7,6), this);
					//updates board
					board[7][7] = 0;
					board[7][5] = 1;
					board[7][4] = 0;
					board[7][6] = 1;
					//updates fields of King
					row = newPair.getRow();
					col = newPair.getCol();
					numMoves += 1;
					return;
				}
				if(newPair.equals(new Pair(7,2))){ //checks for left castling
					Piece rook = coordinates.get(new Pair(7,0)); //checks for the left rook
					
					//updates coordinates map
					coordinates.remove(new Pair(7,0)); 
					coordinates.put(new Pair(7,3), rook);
					coordinates.remove(new Pair(7,4));
					coordinates.put(new Pair(7,2), this);
					//updates board
					board[7][0] = 0;
					board[7][3] = 1;
					board[7][4] = 0;
					board[7][2] = 1;
					//updates fields of King
					row = newPair.getRow();
					col = newPair.getCol();
					numMoves += 1;
					return;
				}
			}
			
			
		}
		//updates coordinates map
		coordinates.remove(original);
		coordinates.put(newPair, this);
		//updates board
		board[original.getRow()][original.getCol()] = 0;
		board[newPair.getRow()][newPair.getCol()] = 1;
		//updates fields after move
		this.row = newPair.getRow();
		this.col = newPair.getCol();
		this.numMoves += 1;
		
	}
	/**
	 * 
	 * @param coordinates HashMap that represents game board
	 * @param newPair coordinates of King
	 * @return returns true if King is in check
	 */
	public boolean inCheck (HashMap<Pair,Piece> coordinates, Pair newPair) {
		for(Pair pair : coordinates.keySet()) {
			Piece piece = coordinates.get(pair); 
			if(!piece.color.equals(this.color)) { //checks if piece has opposite color of King
				if(piece.isValid(coordinates, pair, newPair)) { //checks if piece can capture King
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param coordinates HashMap that represents a game board
	 * @param original original coordinates of King
	 * @param newPair new coordinates of King
	 * @return returns true if valid Castling move
	 */
	public boolean isCastling (HashMap<Pair,Piece> coordinates, Pair original, Pair newPair) {
		if(this.inCheck) { //checks if the king in check
			return false;
		}
		
		if(this.numMoves != 0) { //checks that number of moves the king makes
			return false;
		}
		
		if(this.color.equals("black")) { 
			if(original.row == 0 && original.col == 4) { //checks if black king
				if(newPair.getRow() == 0 && newPair.getCol() == 6) { //checks if right castling
					Piece piece = coordinates.get(new Pair(0,7)); //gets right black rook
					if(piece == null) { // if no piece is there false
						return false;
					}
					if(!(piece instanceof Rook)) { // if piece not rook false
						return false;
					}
					if(piece.numMoves != 0) { //if piece has moved false
						return false;
					}
					if(coordinates.get(new Pair(0,5)) != null || coordinates.get(new Pair(0,6)) != null) { //if pieces between false
						return false;
					}
					
					HashMap<Pair,Piece>table1 = new HashMap<Pair,Piece>(); //checks if king is put in Check during castling movement
					table1.putAll(coordinates);
					HashMap<Pair, Piece> table2 = new HashMap<Pair,Piece> ();
					table2.putAll(coordinates);
					table1.remove(new Pair(0,4));
					table2.remove(new Pair(0,4));
					table1.put(new Pair(0,5), this);
					table2.put(new Pair(0,6), this);
					if(this.inCheck(table1,new Pair(0,5)) || this.inCheck(table2, new Pair (0,6))) {
						return false;
					}
					
					return true;
				}
				if(newPair.getRow() == 0 && newPair.getCol() == 2) { //checks if left castling 
					Piece piece = coordinates.get(new Pair(0,0)); //gets left rook 
					if(piece == null) { //checks if rook is null
						return false;
					}
					if(!(piece instanceof Rook)) { //checks if piece is rook
						return false;
					}
					if(piece.numMoves != 0) { //checks if rook has made any moves
						return false;
					}
					if(coordinates.get(new Pair(0,3)) != null || coordinates.get(new Pair(0,2)) != null) { //checks no pieces between rook and king
						return false;
					}
					
					//checks if during castling king is put in Check
					HashMap<Pair,Piece>table1 = new HashMap<Pair,Piece>();
					table1.putAll(coordinates);
					HashMap<Pair, Piece> table2 = new HashMap<Pair,Piece> ();
					table2.putAll(coordinates);
					table1.remove(new Pair(0,4));
					table2.remove(new Pair(0,4));
					table1.put(new Pair(0,3), this);
					table2.put(new Pair(0,2), this);
					if(this.inCheck(table1, new Pair(0,3)) || this.inCheck(table2, new Pair(0,2))) {
						return false;
					}
					
					return true;
				}
				
			}
			
			return false;
		}
		if (this.color.equals("white")) {
			if(original.row == 7 && original.col == 4) { //checks if white king
				if(newPair.getRow() == 7 && newPair.getCol() == 6) { //checks for right castling
					Piece piece = coordinates.get(new Pair(7,7)); //Retrieves rook
					if(piece == null) { //checks if piece null
						return false;
					}
					if(!(piece instanceof Rook)) { //checks if piece is Rook
						return false;
					}
					if(piece.numMoves != 0) { //checks if rook made a move
						return false;
					}
					
					//checks no pieces between king and rook
					if(coordinates.get(new Pair(7,5)) != null || coordinates.get(new Pair(7,6)) != null) { 
						return false;
					}
					
					//checks if during castling king is put in Check
					HashMap<Pair,Piece>table1 = new HashMap<Pair,Piece>();
					table1.putAll(coordinates);
					HashMap<Pair, Piece> table2 = new HashMap<Pair,Piece> ();
					table2.putAll(coordinates);
					table1.remove(new Pair(7,4));
					table2.remove(new Pair(7,4));
					table1.put(new Pair(7,5), this);
					table2.put(new Pair(7,6), this);
					if(this.inCheck(table1, new Pair(7,5)) || this.inCheck(table2, new Pair(7,6))) {
						return false;
					}
					
					return true;
				}
				if(newPair.getRow() == 7 && newPair.getCol() == 2) { //checks if left castling
					Piece piece = coordinates.get(new Pair(7,0)); //gets left rook
					if(piece == null) { //checks if piece null
						return false;
					}
					if(!(piece instanceof Rook)) { //checks piece is rook
						return false;
					}
					if(piece.numMoves != 0) { //checks number of moves of rook
						return false;
					}
					
					//checks that there are no pieces between the king and rook
					if(coordinates.get(new Pair(7,3)) != null || coordinates.get(new Pair(7,2)) != null) {
						return false;
					}
					
					//checks that king is not put in check during castling
					HashMap<Pair,Piece>table1 = new HashMap<Pair,Piece>();
					table1.putAll(coordinates);
					HashMap<Pair, Piece> table2 = new HashMap<Pair,Piece> ();
					table2.putAll(coordinates);
					table1.remove(new Pair(7,4));
					table2.remove(new Pair(7,4));
					table1.put(new Pair(7,3), this);
					table2.put(new Pair(7,2), this);
					if(this.inCheck(table1, new Pair(7,3)) || this.inCheck(table2, new Pair(7,2))) {
						return false;
					}
					return true;
					
				}
				
			}
			
		}
		
		
		return false;
	}
	/**
	 * @param coordinates HashMap that represents game board
	 * @param original original coordinates of King
	 * @param newPair new coordinates of King
	 * this version is for dummy coordinates map
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		if(this.isCastling(coordinates,original,newPair)) {
			if(original.equals(new Pair (0,4))) { //checks for black king
				if(newPair.equals(new Pair(0,6))) { //executes right castling
					Piece rook = coordinates.get(new Pair(0,7));
					coordinates.remove(new Pair(0,7));
					coordinates.put(new Pair(0,5), rook);
					coordinates.remove(new Pair(0,4));
					coordinates.put(new Pair(0,6), this);
					
					return;
				}
				if(newPair.equals(new Pair(0,2))){ //executes left castling
					Piece rook = coordinates.get(new Pair(0,0));
					coordinates.remove(new Pair(0,0));
					coordinates.put(new Pair(0,3), rook);
					coordinates.remove(new Pair(0,4));
					coordinates.put(new Pair(0,2), this);
				
					return;
				}
			}
			
			if(original.equals(new Pair (7,4))) { //checks for white king
				if(newPair.equals(new Pair(7,6))) { //checks for right castling
					Piece rook = coordinates.get(new Pair(7,7));
					coordinates.remove(new Pair(7,7));
					coordinates.put(new Pair(7,5), rook);
					coordinates.remove(new Pair(7,4));
					coordinates.put(new Pair(7,6), this);
					return;
				}
				if(newPair.equals(new Pair(7,2))){ //checks for left castling
					Piece rook = coordinates.get(new Pair(7,0));
					coordinates.remove(new Pair(7,0));
					coordinates.put(new Pair(7,3), rook);
					coordinates.remove(new Pair(7,4));
					coordinates.put(new Pair(7,2), this);
					return;
				}
			}
			
			
		}
		 //executes coordinates update
		coordinates.remove(original);
		coordinates.put(newPair, this);
		
	}
	

}
