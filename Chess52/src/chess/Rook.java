package chess;

import java.util.HashMap;
/**
 * 
 * @author Shaan Kalola and Tom Murphy
 */
public class Rook extends Piece {
	/**
	 * 
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param numMoves gives the number of Moves the piece made
	 * @param name gives the type of piece 
	 * @param color gives the color of the piece
	 */
	public Rook(int row, int col, int numMoves, String name, String color) {
		super(row, col, numMoves, name, color);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinatesd
	 * @param color gives the color of the piece
	 */
	public Rook (int row, int col, String color) {
		this(row, col, 0, "rook", color);
	}

	
	public boolean isValid(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		//creates new Pair is valid
		if(newPair.getRow()<0 || newPair.getRow() > 7 || newPair.getCol() < 0 || newPair.getCol() > 7) {
			return false;
		}
		//checks that no piece of same color is at the newPair
		if(coordinates.get(newPair) != null && coordinates.get(newPair).color.equals(this.color)) {
			return false;
		}
		//Crates Pair to represent movement from original to newPair
		Pair pair = new Pair (newPair.getRow()- original.getRow(), newPair.getCol() - original.getCol());
		
		//if the rook moves in more than one direction returns false
		if(Math.abs(pair.getRow()) > 0 && Math.abs(pair.getCol()) >0) {
			return false;
		}
		
		//checks for rank movement
		if(pair.getRow() == 0) {
			if(pair.getCol() < 0) {//checks for negative movement along rank
				int boundary = pair.getCol();
				int i = -1;
				//checks no piece on the path
				while(i >= boundary + 1) {
					if(coordinates.get(new Pair (original.getRow(), original.getCol() + i)) != null){
						return false;
					}
					i -= 1;
				}
				//checks no piece on newPair of same color
				Piece piece =coordinates.get(new Pair (original.getRow(), original.getCol() + boundary));
				if(piece != null && piece.color.equals(this.color)) {
					return false;
				}
			}
			if(pair.getCol() > 0) {  //checks for positive movement along rank
				int boundary = pair.getCol();
				int i = 1;
				//checks no piece on the path
				while(i <= boundary-1) {
					if(coordinates.get(new Pair (original.getRow(), original.getCol() + i)) != null){
						return false;
					}
					i += 1;
				}
				//checks no piece on newPair of same color
				Piece piece =coordinates.get(new Pair (original.getRow(), original.getCol() + boundary));
				if(piece != null && piece.color.equals(this.color)) {
					return false;
				}
			}
			
			
		}
		
		if(pair.getCol() == 0) { //checks for file movements
			if(pair.getRow() < 0) { //checks for negative file movement
				int boundary = pair.getRow();
				int i = -1;
				//checks for pieces in path
				while(i >= boundary + 1) {
					if(coordinates.get(new Pair (original.getRow()+i, original.getCol())) != null){
						return false;
					}
					i -= 1;
				}
				//checks that no piece at newPair with same color
				Piece piece = coordinates.get(new Pair (original.getRow()+ boundary, original.getCol()));
				if(piece != null && piece.color.equals(this.color)) {
					return false;
				}
			}
			if(pair.getRow() > 0) { //checks for positive file movement
				int boundary = pair.getRow();
				int i = 1;
				//checks for pieces in path
				while(i <= boundary - 1) {
					if(coordinates.get(new Pair (original.getRow()+i, original.getCol())) != null){
						return false;
					}
					i += 1;
				}
				//checks that no piece at newPair with same color
				Piece piece = coordinates.get(new Pair (original.getRow()+boundary, original.getCol()));
				if(piece != null && piece.color.equals(this.color)) {
					return false;
				}
				
			}
			
			
		}
		
		return true;
	}

	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @param board int [][] that contains 1 where there is a piece and 0 where there is not
	 updates coordinates 
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair, int[][] board) {
		// TODO Auto-generated method stub
		//updates coordinates
		coordinates.remove(original);
		coordinates.put(newPair, this);
		//updates board
		board[original.getRow()][original.getCol()] = 0;
		board[newPair.getRow()][newPair.getCol()] = 1;
		//updates fields
		this.row = newPair.getRow();
		this.col = newPair.getCol();
		this.numMoves += 1;
		
	}

	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 updates coordinates 
	 this method is meant to use on dummy HashMaps that are copies of coordinates, as it doesn't update board, or number of moves
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		//updates coordinates
		coordinates.remove(original);
		coordinates.put(newPair, this);
	}

}
