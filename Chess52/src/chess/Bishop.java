package chess;

import java.util.HashMap;
/**
 * 
 * @author Shaan Kalola and Tom Murphy
 */
public class Bishop extends Piece{
	/**
	 * 
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param numMoves gives the number of Moves the piece made
	 * @param name gives the type of piece 
	 * @param color gives the color of the piece
	 */
	public Bishop(int row, int col, int numMoves, String name, String color) {
		super(row, col, numMoves, name, color);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param row gives the row where the Piece is on the coordinates
	 * @param col gives the column where the Piece is on the coordinates
	 * @param color gives the color of the piece
	 */
	public Bishop (int row, int col, String color) {
		this(row, col, 0, "bishop", color);
	}

	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @return returns true if the piece can move from original to newPair
	 */
	public boolean isValid(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		//checks that newPair valid
		if(newPair.getRow()<0 || newPair.getRow() > 7 || newPair.getCol() < 0 || newPair.getCol() > 7) {
			return false;
		}
		//checks that there is not a piece of the same color at newPair
		if(coordinates.get(newPair) != null && coordinates.get(newPair).color.equals(this.color)) {
			return false;
		}
		
		//representation of movement from original to newPair
		Pair pair = new Pair (newPair.getRow() - original.getRow(), newPair.getCol() - original.getCol());
		
		//checks if movement is diagonal
		if(Math.abs(pair.getCol()) != Math.abs(pair.getRow())) {
			return false;
		}
		
		int boundary = Math.abs(pair.getRow());
		int row = pair.getRow() / boundary;
		int col = pair.getCol() / boundary;
		
		//checks that no pieces along Bishop's movement
		for(int i = 1; i < boundary; i++) {
			Pair p = new Pair(row * i + original.getRow(), col * i + original.getCol());
			Piece piece = coordinates.get(p);
			if(piece != null) {
				return false;
			}
		}
		
		//checks that there is not a piece of the same color at newPair
		Pair p = new Pair(row * boundary + original.getRow(), col * boundary + original.getCol());
		Piece piece = coordinates.get(p);
		if(piece != null && piece.color.equals(this.color)) {
			return false;
		}
		
		
		return true;
	}

	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 @param board int [][] that contains 1 where there is a piece and 0 where there is not
	 updates coordinates 
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair, int[][] board) {
		// TODO Auto-generated method stub
		//updates coordinates HashMap 
		coordinates.remove(original);
		coordinates.put(newPair, this);
		//updates board
		board[original.getRow()][original.getCol()] = 0;
		board[newPair.getRow()][newPair.getCol()] = 1;
		//updates fields
		this.row = newPair.getRow();
		this.col = newPair.getCol();
		this.numMoves += 1;
		
	}


	/**
	 @param coordinates a HashMap that represents the game board
	 @param original a Pair object that represents the original position of the piece on the board
	 @param newPair a Pair object that represents a possible new position of the piece on the board 
	 updates coordinates map for dummy coordinates 
	 */
	public void executeMove(HashMap<Pair, Piece> coordinates, Pair original, Pair newPair) {
		// TODO Auto-generated method stub
		//updates coordinates HashMap
		coordinates.remove(original);
		coordinates.put(newPair, this);
	
		
	}

}
