package chess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
/**
 @author Shaan Kalola and Tom Murphy
 */
public class Chess {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		HashMap<Pair, Piece> coordinates = new HashMap <Pair, Piece> (); //initializes HashMap that represents the Chess Board
		
		
		// Places the black pieces on the board
		
		
		coordinates.put(new Pair(0,0), new Rook (0,0,"black"));
		coordinates.put(new Pair(0,1), new Knight (0,1,"black"));
		coordinates.put(new Pair(0,2), new Bishop(0,2,"black"));
		coordinates.put(new Pair(0,3), new Queen (0,3,"black"));
		coordinates.put(new Pair(0,4), new King(0,4,"black"));
		coordinates.put(new Pair(0,5), new Bishop(0,5, "black"));
		coordinates.put(new Pair(0,6), new Knight(0,6,"black"));
		coordinates.put(new Pair(0,7), new Rook(0,7,"black"));
		
//		Places black pawns on the board
		for(int i = 0; i < 8; i++) {
			coordinates.put(new Pair(1,i), new Pawn(1,i, "black"));
		}
		
		// Places White pieces on the board
		coordinates.put(new Pair(7,0), new Rook (7,0,"white"));
		coordinates.put(new Pair(7,1), new Knight (7,1,"white"));
		coordinates.put(new Pair(7,2), new Bishop(7,2,"white"));
		coordinates.put(new Pair(7,3), new Queen (7,3,"white"));
		coordinates.put(new Pair(7,4), new King(7,4,"white"));
		coordinates.put(new Pair(7,5), new Bishop(7,5, "white"));
		coordinates.put(new Pair(7,6), new Knight(7,6,"white"));
		coordinates.put(new Pair(7,7), new Rook(7,7,"white"));
		
		//Places white pawns on the door
		for(int i = 0; i < 8; i++) {
			coordinates.put(new Pair(6,i), new Pawn(6,i, "white"));
		}
		
		
		//initializes a board that places a one where there is a piece on the board, and places a zero where there is no piece
		int [][] board = new int [8][8];
		
		for(int i = 0; i<2; i++) {
			for(int j = 0; j < 2; j++) {
				board[i][j] = 1;
			}
		}
		
		for(int i = 6; i<8; i++) {
			for(int j = 0; j < 8; j++) {
				board[i][j] = 1;
			}
		}
		
		//Extracts the kings from the Hashmap so we can use them to check for Check and Checkmate
		King blackKing = (King) coordinates.get(new Pair(0,4)); //We keep track of the Kings so that we have access to the inCheck boolean in the King Object
		King whiteKing = (King) coordinates.get(new Pair(7,4)); 
		
		boolean prevMoveDouble = false; // Since en passant can only be performed on the direct next turn, we have to keep track of any pawns that make a double move
		Pawn doublePawn = null; //Variable used to store the pawn that made the last double move
		HashMap <Character,Integer> rowMap = new HashMap<Character,Integer>(); //This Hashmap help converts rank to row numbers
		rowMap.put('8',0);
		rowMap.put('7',1);
		rowMap.put('6', 2);
		rowMap.put('5', 3);
		rowMap.put('4', 4);
		rowMap.put('3', 5);
		rowMap.put('2', 6);
		rowMap.put('1', 7);
		
		HashMap <Character, Integer> colMap = new HashMap <Character,Integer> (); // This Hashmap helps convert file to column numbers
		colMap.put('a', 0);
		colMap.put('b', 1);
		colMap.put('c', 2);
		colMap.put('d', 3);
		colMap.put('e', 4);
		colMap.put('f', 5);
		colMap.put('g', 6);
		colMap.put('h', 7);
		
		String line = null;  //variable used to store player moves

		boolean askDraw = false; // used to determine if previous player asked for draw
		boolean white = true; // used to determine who's turn it is.
		

		Scanner scanner = new Scanner(System.in);		
				
		while(true) { //reads in each line of the Songlib.txt file
			
			
			if(white && whiteKing.inCheck) {  //Checks for Checkmate of White King 
				if(checkMate(coordinates,whiteKing)) {
					printBoard(coordinates);
					System.out.println();
					System.out.println("Checkmate");
					System.out.println("Black wins");
					return;
				}
			}
			
			if(!white && blackKing.inCheck) { //Checks for Checkmate of Black King 
				if(checkMate(coordinates,blackKing)) {
					printBoard(coordinates);
					System.out.println();
					System.out.println("Checkmate");
					System.out.println("Black wins");
					return;
				}
			}
			
			if(white) { //checks for stalemate for white player
				if(stalemate(coordinates, "white")) {
					printBoard(coordinates);
					System.out.println();
					System.out.println("draw");
					return;
				}
			}
			
			if(!white) { //checks for stalemate for black player
				if(stalemate(coordinates, "black")) {
					printBoard(coordinates);
					System.out.println();
					System.out.println("draw");
					return;
				}
			}
			
			printBoard(coordinates); //prints board so player can make next move
			System.out.println();
			if(white) { //determines which player's turn it is
				System.out.print("White's move: ");
			}
			else {
				System.out.print("Black's move: ");
			}
			line = scanner.nextLine(); // accepts next argument 
			if(line.equals("")) {
				break;
			}
			
			
			String [] movement = line.split(" "); //splits original and new position into two separate Strings
			if(resign(movement)) { //Checks for forfeit
				if(white) { //Checks that white player forfeits
					System.out.println("");
					System.out.println("Black wins");
					return;
				}
				else { //Checks that black player forfeits
					System.out.println();
					System.out.println("White wins");
					return;
				}
			}
			
			if(askDraw && draw(movement)) { //Checks if player accepts draw
				System.out.println();
				System.out.println("draw");
				return;
			}
			
			if(!askDraw && draw(movement)) {
				System.out.println("Illegal move, try again");
				System.out.println("");
				continue;
			}
			
			
			Pair original = convert(movement[0],rowMap,colMap); // converts rank file to coordinates for original piece 
			Pair newPair = convert(movement[1],rowMap, colMap); // converts rank file to coordinates for new position
			if(original == null || newPair == null) { //if either the original or newPair is null prints illegal move
				System.out.println("Illegal move, try again");
				System.out.println("");
				continue;
			}
			
			if(original.equals(newPair)) {
				System.out.println("Illegal move, try again");
				System.out.println("");
				continue;
			}
			
			Piece piece = coordinates.get(original); //obtains the Piece at the original coordinates 
			if(piece == null) { //if original piece is null, prints illegal move 
				System.out.println("Illegal move, try again");
				System.out.println("");
				continue;
			}
			if(white && piece.color.equals("black")) { //checks that white player isn't moving a black piece
				System.out.println("Illegal move, try again");
				System.out.println("");
				continue;
			}
			if(!white && piece.color.equals("white")) { //checks that black player isn't moving a white piece
				System.out.println("Illegal move, try again");
				System.out.println("");
				continue;
			}
			boolean isValid = piece.isValid(coordinates, original, newPair); //this method checks if the move is valid under the assumption the King is not in Check
			if(!isValid) { // if not valid then prints error
				System.out.println("Illegal move, try again");
				System.out.println("");
				continue;
			}
			
			
			
			if(white) { //Check if the White King is in Check and if the move made puts it out of check
				HashMap<Pair, Piece> checkMap = new HashMap <Pair, Piece> (); //makes a map that copies the board to check if the move puts the white king in check
				checkMap.putAll(coordinates); // copies coordinates into checkMap
				piece.executeMove(checkMap, original, newPair); //executes move on the checkMap
				boolean inCheck = true;
				if(piece == whiteKing) { //If the piece that moved is king calls the inCheck method in King with the newPair as second parameter, the second parameter in the method is the position of the king
					inCheck = whiteKing.inCheck(checkMap,newPair);
				}
				else {//If the piece that moved is not king calls the inCheck method in King with the King's position as second parameter, the second parameter in the method is the position of the king
					inCheck = whiteKing.inCheck(checkMap,whiteKing.returnPair());
				}
				
				if(inCheck) { //if the move places the king in check, prints Illegal move

					System.out.println("Illegal move, try again");
					System.out.println("");
					continue;
				}
				else {
					whiteKing.inCheck = false; //sets inCheck to false if white king is not in Check
				}
			}
			
			if(!white) { //Check if the Black King is in Check and if the move made puts it out of check
				HashMap<Pair, Piece> checkMap = new HashMap <Pair, Piece> (); //makes a map that copies the board to check if the move puts the black king in check
				checkMap.putAll(coordinates);// copies coordinates into checkMap
				piece.executeMove(checkMap, original, newPair);//executes move on the checkMap
				boolean inCheck = true;
				if(piece == blackKing) {//If the piece that moved is king calls the inCheck method in King with the newPair as second parameter, the second parameter in the method is the position of the king
					inCheck = blackKing.inCheck(checkMap,newPair);
				}
				else { //If the piece that moved is not king calls the inCheck method in King with the King's position as second parameter, the second parameter in the method is the position of the king
					inCheck = blackKing.inCheck(checkMap,blackKing.returnPair());
				}
				if(inCheck) { //if the move places the king in check, prints Illegal move
					System.out.println("Illegal move, try again");
					System.out.println("");
					continue;
				}
				else {
					blackKing.inCheck =false; //sets inCheck to false if blackKing not in check
				}
			}
			
			if(askDraw(movement)) { // Checks if player asks for draw
				askDraw = true;
			}
			else {
				askDraw = false;
			}
			
			
			HashMap <Pair, Piece> evaluatePawnEdgeCase = new HashMap<Pair,Piece>(); //makes a HashMap to evaluate the special moves made by Pawn
			boolean isDoubleMove = false;
			if(piece instanceof Pawn) { //Checks piece is pawn
				evaluatePawnEdgeCase.putAll(coordinates); //copies coordinates
				if(((Pawn) piece).doubleMove(evaluatePawnEdgeCase, original, newPair)) { //checks if piece made a double move
					 isDoubleMove = true;
				}
			}
			
			
			piece.executeMove(coordinates, original, newPair, board); //execute the move, update, board and Hashtable
			System.out.println();
			
			if(doublePawn != null) { //checks if doublePawn is not null that means that in the previous turn it made a double move, but regardless of whether it was deleted in the coordinate map, this means the last move was not a double move, and thus the field must be changed
				doublePawn.lastMovedouble = false; 
				doublePawn = null;
			}
			
			
			if(piece instanceof Pawn) { //Check if the Pawn has been promoted 
				if(isDoubleMove) {
					doublePawn = (Pawn) piece; //sets double pawn to this piece
				}
				
				if(((Pawn) piece).promotion(newPair)) { //checks if pawn has been promoted 
					coordinates.put(newPair, promotePiece(movement,(Pawn)piece)); //promotes pawn
				}
			}
			
			if(white) { //check if black king is in check after white player move 
				if(blackKing.inCheck(coordinates, blackKing.returnPair())) {
					blackKing.inCheck = true;
					System.out.println("Check");
					System.out.println("");
				}
				else {
					blackKing.inCheck = false;
				}
			}
			
			if(!white) { //check if white king is in check after black player move 
				if(whiteKing.inCheck(coordinates, whiteKing.returnPair())) {
					whiteKing.inCheck = true;
					System.out.println("Check");
					System.out.println("");
				}
				else {
					whiteKing.inCheck = false;
				}
			}
			
			
			white = !white; // switches players
			
		}
		
		

	}
	/** @param args - An Array of Strings that contains the user input split by " "
	   @return returns a boolean telling if the player asked for draw 
	 
	 */
	public static boolean askDraw (String [] args) {
		if(args.length != 3) {
			return false;
		}
		if(args[2].equals("draw?")) {
			return true;
		}
		
		
		return false;
	}
	
	
	/** @param args - An Array of Strings that contains the user input split by " "
	   @return returns a boolean telling if the player accepted the draw 
	 
	 */
	public static boolean draw (String args []) {
		if(args.length != 1) {
			return false;
		}
		if(args[0].equals("draw")) {
			return true;
		}
		return false;
	}
	
	/** @param args - An Array of Strings that contains the user input split by " "
	   @return returns a boolean telling if the player resigned
	 
	 */
	public static boolean resign (String [] args) {
		if(args.length != 1) {
			return false;
		}
		if(args[0].equals("resign")) {
			return true;
		}
		return false;
	}
	
	/**
	 @param coordinates is a game board in the form of a HashMap 
	 @param king the king piece that is being checked for checkmate
	 @return returns true if king is in Checkmate, and false if not
	 */
	public static boolean checkMate (HashMap <Pair,Piece> coordinates, King king) { //takes the King that is in Check as a parameter
		Set <Piece> pieces = new HashSet<Piece>();
		
		for(Pair pair : coordinates.keySet()) { //puts every piece that has the same color as the king in a set 
			Piece piece = coordinates.get(pair);
			if(piece.color.equals(king.color)){
				pieces.add(piece);
			}
		}
		
		for(Piece piece : pieces) { 
			Set <Pair> valid = piece.computeAllValidMoves(coordinates,piece.returnPair()); // Computes all Valid Moves of each piece with the Same color as the King
			for (Pair pair : valid) {
				HashMap<Pair,Piece> map = new HashMap<Pair, Piece>(); // makes a dummy coordinate map for us to evaluate possible moves, without updating the primary board
				map.putAll(coordinates);
				boolean isValid = true;
				piece.executeMove(map, piece.returnPair(), pair); // executes move on dummy board
				if(piece == king) { //if the piece is the king, 
					isValid = king.inCheck(map,pair); //if the piece is king, then the second argument is the Valid, as the second arg of inCheck is the King's position
				}
				else {
					isValid = king.inCheck(map,king.returnPair()); // checks if king is in check in dummy coordinate map
				}
				 //checks if king is still in check
				if(!isValid) {
					return false;
				}
			}
		}
		
		return true;
	}
	/**
	 @param coordinates a game board in the format of a HashMap 
	 @param color color of pieces that are being evaluated for stalemate
	 @return returns true if the given color cannot make any moves, false if not
	 */
	public static boolean stalemate (HashMap<Pair,Piece> coordinates, String color) {
		
		Set <Piece> pieces = new HashSet<Piece>();
		
		for(Pair pair : coordinates.keySet()) {
			Piece piece = coordinates.get(pair);
			if(!piece.color.equals(color)) {
				continue;
			}
			Set<Pair> validMoves = piece.computeAllValidMoves(coordinates, pair);
			if(!validMoves.isEmpty()) {
				return false;
			}
			
		}
		
		
		
		return true;
	}
	/**
	 @param args String that is the rank file coordinates of position on the board
	 @param rowMap HashMap that maps rank to row number
	 @param colMap HashMap that maps file to column number
	 @return returns a Pair object that represents the rank file position in a row column format
	 */
	public static Pair convert(String args, HashMap<Character,Integer> rowMap, HashMap <Character,Integer> colMap) {
		if(args == null || (args.length()) != 2) {
			return null;
		}
				
		
		return new Pair(rowMap.get(args.charAt(1)),colMap.get(args.charAt(0)));
		
	}
	/**
	 @param movement a array of Strings that is the user's input split by " "
	 @param pawn a Pawn Object
	 @return returns a Piece object that depends on the user's input 
	 */
	public static Piece promotePiece (String [] movement, Pawn pawn) {
		if(movement.length == 3) {
			String letter = movement[2];
			if(letter.equals("R")) { // if user types R return Rook
				return new Rook(pawn.returnRow(),pawn.returnCol(),pawn.returnNumMoves(), "rook", pawn.returnColor());
			}
			if(letter.equals("N")) { // if user types N return Knight
				return new Knight(pawn.returnRow(),pawn.returnCol(),pawn.returnNumMoves(), "knight", pawn.returnColor());
			}
			if(letter.equals("B")) { // if user types B return Bishop
				return new Bishop(pawn.returnRow(),pawn.returnCol(),pawn.returnNumMoves(), "bishop", pawn.returnColor());
			}
		}
		
		// if user types anything else returns a Queen object
		return new Queen(pawn.returnRow(),pawn.returnCol(),pawn.returnNumMoves(), "queen", pawn.returnColor());
		
	}
	
	/**
	 @param coordinates a representation of the game board as a HashMap
	 <p>prints the board </p>
	 */
	public static void printBoard (HashMap<Pair,Piece> coordinates) {
		boolean ishash = false; 
		for(int i = 0; i < 8; i++) {
			if(i%2 == 0) { //determines whether to print hashes if no piece at coordinate position
				ishash = false;
			}
			else {
				ishash = true;
			}
			for(int j = 0; j < 8; j++) { //loops through entire board
				Piece piece = coordinates.get(new Pair(i,j)); //finds piece at coordinates
				if(piece != null) {
					System.out.print(pieceId(piece) + " "); //prints out piece idea
				}
				else if (ishash) { //prints hash id ishash
					System.out.print("## ");
				}
				else { //prints "   " if !ishash
					System.out.print("   ");
				}
				ishash = !ishash;
			}
			System.out.println(8-i); // prints the rank
		}
		System.out.println(" a  b  c  d  e  f  g  h"); //prints the file
	}
	/**
	 @param piece piece that's id is being evaluated
	 @return returns a two character code for each piece
	 */
	public static String pieceId( Piece piece) {
		String color = null;
		String type = null;
		if(piece.color.equals("white")) { //determines the color of the piece 
			color = "w";
		}
		else {
			color = "b";
		}
		
		//determines the type of the Piece
		if(piece instanceof Pawn) {
			type = "p";
		}
		else if (piece instanceof Rook) {
			type = "R";
		}
		else if (piece instanceof Knight) {
			type = "N";
		}
		else if (piece instanceof Bishop) {
			type = "B";
		}
		else if (piece instanceof Queen) {
			type = "Q";
		}
		else {
			type = "K";
		}
		
		
		
		return color+type;
	}

}
